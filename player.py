from agent import Agent
from res.config import WIN_HEIGHT, WIN_WIDTH

class Player(Agent):
    def __init__(self):
        super().__init__()
        self.set_res_path("res/spaceship.png")
        self.set_XY_width_height(WIN_HEIGHT/2, WIN_WIDTH/2, 36, 36)
