from agent import Agent

class Bullet(Agent):
    def __init__(self):
        super().__init__()
        self.set_res_path("res/blue_laser.jpg")
        self.set_XY_width_height(100, 100, 10, 20)

