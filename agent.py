from PySide2.QtWidgets import QApplication, QMainWindow, QGraphicsScene, QPushButton, QGraphicsView, QGraphicsItem, \
    QGraphicsRectItem, QStyleOptionGraphicsItem
from PySide2.QtGui import QBrush, QPen, QFont, QPainter, QPixmap
from PySide2.QtCore import Qt, QTimer, QTime, QRectF, QRect
import sys


class Agent(QGraphicsItem):
    def __init__(self):
        super().__init__()
        self._x = 0
        self._y = 0
        self._width = 0
        self._height = 0
        self._res_path = "res/spaceship.png"
        self.setActive(True)

    def boundingRect(self):
        return QRectF(self._x, self._y, self._width, self._height)

    def set_XY_width_height(self, X, Y, w, h):
        self._x = X
        self._y = Y
        self._width = w
        self._height = h

    def set_res_path(self, res_path):
        self._res_path = res_path

    def paint(self, painter, option, widget):
        rect = self.boundingRect().getRect()
        painter.drawPixmap(*rect, QPixmap(self._res_path))

    def move(self, x, y):
        self._x += x
        self._y += y

    def get_y(self):
        return self._y

    def get_x(self):
        return self._x





