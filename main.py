from PySide2.QtWidgets import QApplication, QMainWindow, QGraphicsScene, QPushButton, QGraphicsView, QGraphicsItem
from PySide2.QtGui import QBrush, QPen, QFont
from PySide2.QtCore import Qt, QTimer, QTime

import sys
from res.config import WIN_WIDTH, WIN_HEIGHT
from player import Player
from alien import Alien
from bullet import Bullet
from random import randint
import threading
from playsound import playsound


class Window(QMainWindow):
    def __init__(self):
        super().__init__()

        self.setWindowTitle("Pyside2 Space Inv")
        self.setGeometry(0, 0, WIN_WIDTH, WIN_HEIGHT)
        self.create_ui()
        self.show()
        self.time = QTime()
        self.timer = QTimer()
        self.timer.setInterval(1 / 30)
        self.timer.timeout.connect(self.update)
        self.timer.start()
        self.bullet = 0
        self.spaceship_bullet_list = []
        self.alien_bullet_list = []
        self.alien_list = []
        self.game_over = False
        self.direction = 0
        self.direction_timer = threading.Timer(2, self.change_direction)
        self.direction_timer.start()



    def change_direction(self):
        if self.direction == 1:
            self.direction = 0
        else:
            self.direction = 1
        self.direction_timer = threading.Timer(2, self.change_direction)
        self.direction_timer.start()

    def create_ui(self):
        self.scene = QGraphicsScene(self)
        self.view = QGraphicsView(self.scene, self)
        self.view.setGeometry(0, 0, WIN_WIDTH, WIN_HEIGHT)
        self.scene.setItemIndexMethod(QGraphicsScene.NoIndex)
        self.view.setFixedSize(WIN_WIDTH, WIN_HEIGHT)
        self.view.setSceneRect(0, 0, WIN_WIDTH, WIN_HEIGHT)
        self.view.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.view.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.spaceship = Player()
        self.scene.addItem(self.spaceship)

    def keyPressEvent(self, event) -> None:
        self.handle_key_event(event)

    def handle_key_event(self, event) -> None:
        if event.key() == Qt.Key_D:
            self.spaceship.move(10, 0)
        if event.key() == Qt.Key_W:
            self.spaceship.move(0, -10)
        if event.key() == Qt.Key_A:
            self.spaceship.move(-10, 0)
        if event.key() == Qt.Key_S:
            self.spaceship.move(0, 10)
        if event.key() == Qt.Key_Space:
            S = threading.Thread(target=self.play_sound_player_shoot)
            S.start()
            self.spaceship_bullet_list.append(Bullet())
            self.spaceship_bullet_list[-1].set_XY_width_height(self.spaceship.get_x() + 18, self.spaceship.get_y() + 18,
                                                               10, 20)
            self.scene.addItem(self.spaceship_bullet_list[-1])

    def play_sound_player_shoot(self):
        playsound('res/shoot.mp3')

    def play_sound_alien_shoot(self):
        playsound('res/shoot_alien.mp3')

    def play_sound_player_dies(self):
        playsound('res/death.mp3')

    def play_sound_alien_dies(self):
        playsound('res/death.mp3')

    def check_enemy_limit(self):
        return len(self.alien_list) + 1 <= 6

    def spawn_aliens(self):
        if not self.check_enemy_limit():
            return None

        x = randint(100, WIN_WIDTH - 100)
        alien = Alien()
        alien.set_XY_width_height(x, 0, 36, 36)
        self.scene.addItem(alien)

        if not alien.collidingItems(Qt.IntersectsItemShape):
            self.alien_list.append(alien)
        else:
            self.scene.removeItem(alien)

    def check_bullet_collision(self):
        for bullet in self.spaceship_bullet_list:
            for alien in self.alien_list:
                if alien in bullet.collidingItems():
                    self.spaceship_bullet_list.remove(bullet)
                    self.scene.removeItem(bullet)
                    self.alien_list.remove(alien)
                    self.scene.removeItem(alien)
                    S = threading.Thread(target=self.play_sound_alien_dies)
                    S.start()

    def game_over_notification(self):
        S = threading.Thread(target=self.play_sound_player_dies())
        S.start()
        print("GAME OVER")
        self.game_over = True

    def check_player_death(self):
        for bullet in self.alien_bullet_list:
            if self.spaceship in bullet.collidingItems():
                self.game_over_notification()

        for alien in self.alien_list:
            if alien.get_y() > 890:
                self.game_over_notification()

    def update_bullets(self):
        for bullet in self.spaceship_bullet_list:
            if bullet and bullet.get_y() >= 0:
                bullet.move(0, -0.3)
            if bullet.get_y() <= 0:
                self.spaceship_bullet_list.remove(bullet)
                self.scene.removeItem(bullet)

        for bullet in self.alien_bullet_list:
            if bullet and bullet.get_y() < 890:
                bullet.move(0, 0.3)
            if bullet.get_y() > 890:
                self.alien_bullet_list.remove(bullet)
                self.scene.removeItem(bullet)

    def update_aliens(self):
        for alien in self.alien_list:
            if alien.get_y() < 900:
                alien.move(0, 0.015)
            if self.direction:
                alien.move(0.015, 0)
            else:
                alien.move(-0.015, 0)

            if randint(0, 7000) == 69:
                self.alien_bullet_list.append(Bullet())
                self.alien_bullet_list[-1].set_res_path("res/red_laser.png")
                self.alien_bullet_list[-1].set_XY_width_height(alien.get_x() + 15, alien.get_y() + 15, 10, 20)
                self.scene.addItem(self.alien_bullet_list[-1])
                shoot_thread = threading.Thread(target=self.play_sound_alien_shoot)
                shoot_thread.start()

    def update(self):
        if not self.game_over:
            self.time.start()
            self.view.scene().update()
            self.update_bullets()
            self.check_bullet_collision()
            self.spawn_aliens()
            self.update_aliens()
            self.check_player_death()



app = QApplication(sys.argv)
window = Window()

sys.exit(app.exec_())
