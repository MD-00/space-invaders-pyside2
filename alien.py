from agent import Agent
from res.config import WIN_HEIGHT, WIN_WIDTH


class Alien(Agent):
    def __init__(self):
        super().__init__()
        self.set_res_path("res/alien.png")
        self.set_XY_width_height(0, 0, 36, 36)
        self.direction = 1
        self.direction_counter = 0

    def get_direction(self):
        return self.direction

    def set_direction(self, direction):
        self.direction = direction

    def get_direction_counter(self):
        return self.direction_counter

    def add_direction_counter(self):
        self.direction_counter += 1
